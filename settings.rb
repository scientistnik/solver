require_relative 'attribute.rb'

class Settings < Attribute
	def cost attr
		return name if empty?
		sum = 0
		each_pair do |par, s|
			next if !attr.key? par

			val = s.name
			if !s.empty?
				if s.key? attr[par].name.to_sym
					val *= s[attr[par].name.to_sym].cost attr[par]
				else
					val = 0
				end
			end
			sum += val
			#puts "sum[#{par}]=#{val}"
		end
		sum
	end
end
