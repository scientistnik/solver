#!/usr/bin/env ruby

require_relative 'settings.rb'

pm = nil
param = {}
ARGV.each do |arg|
	if arg == "-s"
		pm = :settings
	elsif !pm.nil?
		param[pm] = arg
		pm = nil
	else
		param[:load] = arg
	end
end

stg = eval File.open(param[:settings],'r').read

atts = eval("#{stg.name}=Attribute.new '#{stg.name}'\n#{File.open(param[:load],'r').read}\n#{stg.name}")

p stg
p atts


result = {}
atts.each_pair { |name,obj| result[name] = stg.cost obj }

p result

