class Attribute < Hash
	attr_reader :name, :parent

	def initialize name, parent=nil, &block
		@name = name
		@parent = parent
		self.instance_eval(&block) if block_given?
	end

	def inspect
		if self.empty?
			@name.to_s
		else
			@name.to_s + super
		end
	end

	def new name, &block
		self[name]= self.class.new name, self, &block
	end

	alias_method :новый, :new

	def method_missing name, value=nil, &block
		super if value.nil?
		self[name]= self.class.new value, self, &block
	end
end
